terraform {
  backend "s3" {
    bucket = "myziyotek-training-597429366363-us-east-1"
    key    = "test/another-ci-cd.tfstate"
    region = "us-east-1"
    # dynamodb_table = "terraform-lock"
  }
}
